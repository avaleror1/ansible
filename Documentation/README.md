# Use Case Documentation

This folder contains the documentation of all Use Cases and scenarios.

:warning: ***WARNING*** Be aware that although the demo environment is fully functionally and you can indeed order and deploy resources, deployment times are slower due to the technology used under the hood. Keep this in mind when demoing and do not waste time by waiting for an provisioning order to fully complete.

:heavy_check_mark: ***NOTE*** It is highly recommend to test the use cases you want to demonstrate in advance, Make yourself familiar with the environment and how things are implemented. Do not try to diverse into something you never checked yourself because you might confuse your audience.

## Ansible Tower

As part of the lab environment an Ansible Tower is available. It is preconfigured with some Job Templates.

[Details about Ansible Tower](ansible-tower.md)
